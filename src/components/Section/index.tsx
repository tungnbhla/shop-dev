import { Box, Flex, Text } from "@chakra-ui/react";
import PropTypes from "prop-types";
import type { FC, ReactNode } from "react";
import Image from "next/image";

interface SectionProps {
  children?: ReactNode;
}

export const Section: FC<SectionProps> = (props) => {
  const { children } = props;
  return (
    <Box height="100%" w="100%" position="relative">
      {/* <Flex align="center" justifyContent={"left"} marginTop={"60px"}>
        <Flex w="100%" height="5px" width="40px" background={"#015ADE"} mr="8px" />
        <Flex fontSize="32px" fontWeight={700}>Special Product.</Flex>
      </Flex> */}
      {children}
    </Box>
  );
};

Section.propTypes = {
  children: PropTypes.node,
};

/* eslint-disable react/no-unescaped-entities */
import { Box, Flex, Text } from "@chakra-ui/react";
import PropTypes from "prop-types";
import type { FC, ReactNode } from "react";
import Image from "next/image";

export const WeLoveOurClients: FC = () => {
  return (
    <Flex height="100%" w="100%" flexDirection={"column"} textAlign={"center"} justifyContent={"center"} position="relative">
        <Text fontSize={"15px"}>CLIENT SAY'S</Text>
        <Text fontSize={"48px"}>We Love Our Clients</Text>
    </Flex>
  );
};

WeLoveOurClients.propTypes = {
  children: PropTypes.node,
};

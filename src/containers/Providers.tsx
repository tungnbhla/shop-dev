// app/providers.tsx
"use client";

import { CacheProvider } from "@chakra-ui/next-js";
import { ChakraProvider } from "@chakra-ui/react";
import { Meta } from "./Meta";

export function Providers({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Meta title="Genuine" description="" />
      <CacheProvider>
        <ChakraProvider>{children}</ChakraProvider>
      </CacheProvider>
    </>
  );
}

import "../styles/globals.css";
import type { AppProps } from "next/app";
import { Providers } from "../containers/Providers";
import { ReactNode } from "react";

function MyApp({ Component, pageProps }: AppProps) {
  const getLayout = Component.getLayout ?? ((page: ReactNode) => page);
  return (
    <Providers>
      {getLayout(<Component {...pageProps} />)}
    </Providers>
  );
}

export default MyApp;

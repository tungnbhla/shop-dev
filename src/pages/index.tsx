import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { Layout } from "../layout";
import { ReactNode, useEffect, useState } from "react";
import { Section } from "../components/Section";
import axios from "axios";
import { WeLoveOurClients } from "../containers/Home/components/WeLoveOurClients";

interface UserData {
  name: string;
  description: string;
  img: string;
}

const Index = () => {
  const [userData, setUserData] = useState<UserData[]>([]);
  useEffect(() => {
    axios
      .get(`http://192.168.33.100:3001/cousers`)
      .then((response) => setUserData(response.data))
      .catch((error) => console.error("Error fetching user data:", error));
  }, []);
  console.log("userData :>> ", userData);
  const data = userData || {};
  return (
    <div className={styles.container}>
      <Section>
        <main className={styles.main}>
          <h1 className={styles.title}>
            {/* Welcome to <a href="https://nextjs.org">Tùng!</a> */}
            Coming soon
          </h1>
          <div style={{ display: "flex" }}>
            {data ? (
              data.map((e) => {
                return (
                  <div style={{ margin: "0 10px 0 0" }} key={e.name}>
                    <img src={e.img} alt={e.name} />
                    <div>{e.name}</div>
                    <div>{e.description}</div>
                  </div>
                );
              })
            ) : (
              <>No data</>
            )}
          </div>

          {/* <p className={styles.description}>
            Get started by editing{" "}
            <code className={styles.code}>pages/index.tsx</code>
          </p> */}

          {/* <div className={styles.grid}>
            <a href="https://nextjs.org/docs" className={styles.card}>
              <h2>Documentation &rarr;</h2>
              <p>Find in-depth information about Next.js features and API.</p>
            </a>

            <a href="https://nextjs.org/learn" className={styles.card}>
              <h2>Learn &rarr;</h2>
              <p>Learn about Next.js in an interactive course with quizzes!</p>
            </a>

            <a
              href="https://github.com/vercel/next.js/tree/master/examples"
              className={styles.card}
            >
              <h2>Examples &rarr;</h2>
              <p>Discover and deploy boilerplate example Next.js projects.</p>
            </a>

            <a
              href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
              className={styles.card}
            >
              <h2>Deploy &rarr;</h2>
              <p>
                Instantly deploy your Next.js site to a public URL with Vercel.
              </p>
            </a>
          </div> */}
        </main>
      </Section>

      {/* <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer> */}
    </div>
  );
};

Index.getLayout = (page: ReactNode) => <Layout>{page}</Layout>;

export async function getStaticProps() {
  return {
    props: {},
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 60 * 60 * 24, // In days
  };
}

export default Index;

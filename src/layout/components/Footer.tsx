/* eslint-disable react/no-children-prop */
import {
  Box,
  Flex,
  Input,
  InputGroup,
  InputRightAddon,
  Text,
} from "@chakra-ui/react";
import Image from "next/image";
import PropTypes from "prop-types";

export const Footer = () => {
  const itemFooter = [
    {
      item: [
        { name: "My account", path: "", value: "key" },
        { name: "Your account", path: "" },
        { name: "Return center", path: "" },
        { name: "Purchase", path: "" },
        { name: "App download", path: "" },
      ],
    },
    {
      item: [
        { name: "My account", path: "", value: "key" },
        { name: "Your account", path: "" },
        { name: "Return center", path: "" },
        { name: "Purchase", path: "" },
        { name: "App download", path: "" },
      ],
    },
    {
      item: [
        { name: "Get Latest Updates", path: "", value: "key" },
        {
          name: "Get Latest Updates There are many variations of passages Lorem Ipsum available",
          path: "",
        },
        { name: "Send email", path: "" },
      ],
    },
  ];
  const a = [{ a: "e", b: "e" }];
  return (
    <Flex
      w="100%"
      fontSize={"14px"}
      fontWeight={400}
      align="center"
      justifyContent={{ base: "center", md: "center", lg: "left" }}
      px={{ base: "0px", lg: "20px" }}
      py="48px"
    >
      <Flex flexDirection={"column"} width="25%">
        <Image width={154} height={47} src="/logo-full.png" alt="" />
        <Text mt="24px">
          502 New design str, melbourne, san francisco, CA 94110, united states
          of america​.
        </Text>
        <Text>(+01) 123-456-789</Text>
        <Text>mercenary.newbie@gmail.com</Text>
      </Flex>
      <Flex width="75%" justifyContent={"space-evenly"}>
        {itemFooter.map((e: any) => {
          const listItem = e.item;
          return (
            <Flex key={e.index} flexDirection={"column"} maxWidth="30%">
              {listItem.map((item: any) => {
                return (
                  <>
                    <Text
                      key={item.name}
                      fontSize={item.value === "key" ? "24px" : ""}
                      mb={item.value === "key" ? "24px" : ""}
                    >
                      {item.name}
                    </Text>
                    {item.name === "Send email" ? (
                      <InputGroup w="100%" mt="12px">
                        <Input placeholder="Email..." />
                        <InputRightAddon
                          backgroundColor={"rgb(84, 193, 201)"}
                          color="black"
                          children="Send"
                        />
                      </InputGroup>
                    ) : (
                      ""
                    )}
                  </>
                );
              })}
            </Flex>
          );
        })}
      </Flex>
    </Flex>
  );
};

Footer.propTypes = {
  children: PropTypes.node,
};

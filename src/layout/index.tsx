import { Box, Flex, Show, Text } from "@chakra-ui/react";
import PropTypes from "prop-types";
import type { FC, ReactNode } from "react";
import Image from "next/image";
import { Footer } from "./components/Footer";

interface LayoutProps {
  children?: ReactNode;
}

export const Layout: FC<LayoutProps> = (props) => {
  const { children } = props;
  return (
    <Box
      height="100%"
      minH={"100vh"}
      display="flex"
      flexDirection="column"
      justifyContent={"space-between"}
      position="relative"
    >
      <Flex width={"100%"} display={"block"}>
        <Flex
          height="40px"
          justifyContent={"center"}
          align="center"
          backgroundColor="rgb(84, 193, 201)"
        >
          <Text
            maxWidth={"1536px"}
            textAlign={"center"}
            fontWeight="700"
            fontSize={"13px"}
          >
            Nhập CM623APL Tặng Áo polo cho đơn từ 699k{" "}
          </Text>
        </Flex>
        <Flex
          borderBottom="1px solid #e4e4e4"
          height={"72px"}
          width="100%"
          px={{ base: "20px", lg: "20px" }}
        >
          <Flex
            justifyContent={"space-between"}
            align="center"
            maxWidth={"1536px"}
            w="100%"
            mx="auto"
          >
            <Image src="/logo-full.png" width={154} height={47} alt="" />
            <Flex fontWeight="600" fontSize="16px" align={"center"}>
              <Image
                width={32}
                height={32}
                src="/image/icons/vietnam.png"
                alt=""
              />
              <Text ml={"12px"}>VNĐ</Text>
            </Flex>
          </Flex>
        </Flex>
        <Show above="lg">
          <Flex
            px={{ base: "20px", lg: "20px" }}
            w="100%"
            height={"66px"}
            borderBottom="1px solid #e4e4e4"
            align="center"
            fontWeight={600}
          >
            <Flex
              justifyContent={"space-between"}
              maxWidth={"1536px"}
              w="100%"
              mx="auto"
            >
              <Flex>
                <Text mr={"24px"}>Home</Text>
                <Text mr={"24px"}>Shop</Text>
                <Text mr={"24px"}>Service</Text>
                <Text mr={"24px"}>About Us</Text>
                <Text mr={"24px"}>Contact Us</Text>
              </Flex>
              <Flex>
                <Flex mr={"24px"}>
                  {" "}
                  <Image
                    src="/image/icons/user.png"
                    width={24}
                    height={24}
                    alt=""
                  />
                  <Text ml="8px">Sign In</Text>
                </Flex>
                <Flex mr={"24px"}>
                  {" "}
                  <Image
                    src="/image/icons/header-heart.svg"
                    width={24}
                    height={24}
                    alt=""
                  />
                  <Text ml="8px">Wishlist</Text>
                </Flex>
                <Flex>
                  {" "}
                  <Image
                    src="/image/icons/shopping-cart.png"
                    width={24}
                    height={24}
                    alt=""
                  />
                  <Text ml="8px">Card</Text>
                </Flex>
              </Flex>
            </Flex>
          </Flex>
        </Show>
      </Flex>
      {children}
      <Flex backgroundColor={"#151515"} color={"white"}>
        <Box maxWidth={"1536px"} w="100%" mx="auto">
          <Footer />
          <Flex>
            <Flex
              w="100%"
              borderTop="1px solid #e4e4e4"
              height="55px"
              fontSize={"14px"}
              fontWeight={500}
              fontStyle={"italic"}
              align="center"
              justifyContent={{ base: "center", md: "center", lg: "left" }}
              mx={{ base: "0px", lg: "20px" }}
            >
              <Text>Genuine 2023 Copyright</Text>
            </Flex>
          </Flex>
        </Box>
      </Flex>
    </Box>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
};
